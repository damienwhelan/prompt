import os
from random import randint

path = os.path.abspath(os.path.dirname(__file__))

f = open(os.path.join(path, "Style"), "r")
g = open(os.path.join(path, "Action"), "r")
h = open(os.path.join(path, "Word"), "r")

lines_f = f.readlines()
lines_g = g.readlines()
lines_h = h.readlines()

pos_S = randint(0, len(lines_f))
pos_A = randint(0, len(lines_g))
pos_W = randint(0, len(lines_h))

print("\n" + lines_f[pos_S] + lines_g[pos_A] + lines_h[pos_W])

f.close()
g.close()
h.close()
